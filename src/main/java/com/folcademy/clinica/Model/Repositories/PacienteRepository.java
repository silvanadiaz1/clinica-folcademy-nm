package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("pacienteRepository")
public interface PacienteRepository extends JpaRepository<Paciente, Integer>
{
}
