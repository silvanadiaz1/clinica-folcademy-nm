package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.ErrorMessage;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TurnoService implements ITurnoService
{
    private final TurnoRepository turnoRepository;
    private final MedicoRepository medicoRepository;
    private final PacienteRepository pacienteRepository;
    private final TurnoMapper turnoMapper;


    public TurnoService(TurnoRepository turnoRepository, MedicoRepository medicoRepository, PacienteRepository pacienteRepository, TurnoMapper turnoMapper) {
        this.turnoRepository = turnoRepository;
        this.medicoRepository = medicoRepository;
        this.pacienteRepository = pacienteRepository;
        this.turnoMapper = turnoMapper;
    }

    @Override
    public List<TurnoEnteroDto> listarTodos()
    {
        List<TurnoEnteroDto> a= new ArrayList<>();
        a=turnoRepository.findAll().stream().map(turnoMapper::entityToDto).collect(Collectors.toList());
        if(a.isEmpty())
        {
            throw new NotFoundException(ErrorMessage.Turnos_Not_exist);
        }else return a;
    }

    public TurnoEnteroDto listarUno (Integer id)
    {
        TurnoEnteroDto a = turnoRepository.findById(id).map(turnoMapper::entityToDto).orElse(null);
        if (a==null)
        {
            throw new NotFoundException(ErrorMessage.Turnos_Not_exist);

        } else return a;

    }

    public TurnoEnteroDto agregar (TurnoEnteroDto entity)
    {
        if (medicoRepository.existsById(entity.getIdmedico()))
        {
            if (pacienteRepository.existsById(entity.getIdpaciente()))
            {
                entity.setIdturno(null);
                return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity((entity))));

            }
            else throw new BadRequestException(ErrorMessage.Paciente_Not_exist);
        }
        else throw new NotFoundException(ErrorMessage.Medico_Not_exist);
    }

    public TurnoEnteroDto editar (Integer idturno, TurnoEnteroDto dto)
    {
        if(!turnoRepository.existsById(idturno)) // esto pregunta si existe el medico por la id//
        {
            throw new NotFoundException(ErrorMessage.Turno_Not_exist);
        }else
            dto.setIdturno(idturno);
        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(dto)));
    }
    public Boolean eliminar (Integer idturno)
    {
        if (!turnoRepository.existsById(idturno))
        {
            throw new NotFoundException(ErrorMessage.Turno_Not_exist);
        }else
            turnoRepository.deleteById(idturno);
        return true;////

    }////
}
