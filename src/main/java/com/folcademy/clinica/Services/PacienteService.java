package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.ErrorMessage;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service("pacienteService")
public class PacienteService implements IPacienteService
{
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;


//inyeccion de dependencia
    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper)
    {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }

    //acá hacemos la implementacion de los metodos declarados en el IPacienteService
    @Override
    public List<PacienteEnteroDto> listarTodos()
    {
        List<PacienteEnteroDto> a=new ArrayList<>();
        a=pacienteRepository.findAll().stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());
        if(a.isEmpty())
        {
            throw new NotFoundException(ErrorMessage.Pacientes_Not_exist);
        }
        else return a;
    }

    @Override
    public PacienteEnteroDto listarUno(Integer id)
    {
        PacienteEnteroDto paciente=pacienteRepository.findById(id).map(pacienteMapper::entityToDto).orElse(null);
        if (Objects.isNull(paciente))
        {
            throw new NotFoundException(ErrorMessage.Paciente_Not_exist);
        }
        return paciente;
    }

    public PacienteEnteroDto agregar(PacienteEnteroDto entity)
    {
        if (Objects.isNull(entity.getDni()))
        {
            throw new BadRequestException(ErrorMessage.DNI_Not_Null);
        }
        else
        entity.setIdpaciente(null);
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(entity)));
    }

    public PacienteEnteroDto editar(Integer idPaciente, PacienteEnteroDto dto)
    {
        if(!pacienteRepository.existsById(idPaciente))
        {
            throw new BadRequestException(ErrorMessage.Paciente_Not_exist);
        }else
            dto.setIdpaciente(idPaciente);
        return pacienteMapper.entityToEnteroDto(pacienteRepository.save(pacienteMapper.enteroDtoToEntity(dto) ) );
    }

    public boolean editarTelefono(Integer idPaciente, String telefono )
    {
        if(pacienteRepository.existsById(idPaciente))
        {
            Paciente entity=pacienteRepository.findById(idPaciente).orElse(new Paciente());
            entity.setTelefono(telefono);
            pacienteRepository.save(entity);
            return true;
        }
        return false;
    }

    public Boolean eliminar(Integer id)
    {
        if(!pacienteRepository.existsById(id))
        {
            throw new NotFoundException(ErrorMessage.Paciente_Not_exist);
        }else
            pacienteRepository.deleteById(id);
        return true;
    }


}
