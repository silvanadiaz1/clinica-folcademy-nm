package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;
    public PacienteController(PacienteService pacienteService)
    {
        this.pacienteService = pacienteService;
    }


    @GetMapping(value = "")
    public ResponseEntity<List<PacienteEnteroDto>> ListarTodo()
    {
        return ResponseEntity.ok().body(pacienteService.listarTodos());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PacienteEnteroDto> listarUno(@PathVariable(name = "id") Integer id)
    {
        return ResponseEntity.ok().body(pacienteService.listarUno(id));
    }

    @PostMapping("")
    public ResponseEntity<PacienteEnteroDto> agregar(@RequestBody @Validated PacienteEnteroDto dto)
    {
        return ResponseEntity.ok(pacienteService.agregar(dto));
    }

    @PutMapping("/{idPaciente}") public ResponseEntity<PacienteEnteroDto> editar(@PathVariable(name="idPaciente") int id, @RequestBody PacienteEnteroDto dto)
    {
        return ResponseEntity.ok(pacienteService.editar(id,dto));
    }

    @DeleteMapping("/{idPaciente}")public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idPaciente")Integer id)
    {
        return ResponseEntity.ok(pacienteService.eliminar(id));
    }

}
